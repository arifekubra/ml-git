import numpy as np

alpha = 0.5
newFile = open('readme.txt', 'w', encoding="utf8")

# compute sigmoid nonlinearity
def sigmoid(x):
    output = 1 / (1 + np.exp(-x))
    return output


# convert output of sigmoid function to its derivative
def sigmoidDerivative(output):
    return output * (1 - output)


inputArray=[]
with open('input.txt') as f:
    for line in f:
        for ch in line:
            if(ch != '\n'):
               inputArray.append(int(ch))

input = np.array(list(inputArray)).reshape(16, 16)

outputArray=[]
with open('output.txt') as f:
    for line in f:
        for ch in line:
            if(ch != '\n'):
               inputArray.append(int(ch))

output = np.array(list(inputArray)).reshape(16, 16)


np.random.seed(1)

# randomly initialize our weights between -0.5 to 0.5
firstWeights = np.random.random((16, 4)) - .5
secondWeights = np.random.random((4, 16)) - .5

for j in range(20):
    for i in range(16):
        # Feed forward through layers 0, 1, and 2
        inputLayer = np.array([input[i]])

        # Find output of layers
        #(1x4)
        hiddenLayer = np.array(sigmoid(np.dot(inputLayer, firstWeights)))
        #1x16
        outputLayer =  np.array(sigmoid(np.dot(hiddenLayer, secondWeights)))

       #part 1.1: output-target(1x16)
        outputLayer_findingError1 = outputLayer - output[i]

        #part 1.2: (output-target)*(derivative of sigmoid)(1x16)
        outputLayer_findingError2 = outputLayer_findingError1 * sigmoidDerivative(outputLayer)

        #find derivative of total error between hidden and output layer (4x16)
        outputLayer_totalError = hiddenLayer.T.dot(outputLayer_findingError2)

        #part 2.1: derivative of total error of output layer * weights of output-hidden layer(1x4)
        hiddenLayer_fingdingError1 = outputLayer_findingError2.dot(secondWeights.T)

       #part 2.2: multiply the result the derivative of hiddenlayer (1x4)
        hiddenLayer_fingdingError2 = hiddenLayer_fingdingError1 * sigmoidDerivative(hiddenLayer)

       #find derivative of total error between input and hidden layer (16x4)
        hiddenLayer_totalError = inputLayer.T.dot(hiddenLayer_fingdingError2)


        #update weights
        secondWeights -= alpha * outputLayer_totalError
        firstWeights -= alpha * hiddenLayer_totalError





